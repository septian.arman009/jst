 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data_table extends CI_Model {

 function Datatables_User($dt) {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $sql = "SELECT {$columns} FROM {$dt['table']}";
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';
        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $where .= $columnd[$i] . ' LIKE "%' . $search . '%"';

                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }
        if ($where != '') {
            $sql .= " WHERE " . $where;
        }
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        foreach ($list->result_array() as $row => $value) {
            $rows = array();
            $rows[] = 'SU-' . $value['id_user'];
            $rows[] = $value['nama'];
            $rows[] = $value['username'];
            $rows[] = $value['email'];
            $rows[] = '<a class="btn btn-info btn-xs" title="Ubah" id="pointer" onclick="editOpen(' . "'" . $value["id_user"] . "'" . ');">
            <i class="glyphicon glyphicon-edit"></i></a> | 
            <a title="Hapus" class="btn btn-danger btn-xs delete' . $value["id_user"]  . '" id="pointer" onclick="deleteUser(' . "'" . $value["id_user"] . "'" . ');">
            <i class="glyphicon glyphicon-trash" ></i></a>
            <a id="delete' . $value["id_user"]  . '" style="display: none;"><i class="glyphicon glyphicon-refresh"></i></a>';

            $option['data'][] = $rows;
        }
        echo json_encode($option);
    }

    function Datatables_bobot($dt) {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $sql = "SELECT {$columns} FROM {$dt['table']}";
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';
        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $where .= $columnd[$i] . ' LIKE "%' . $search . '%"';

                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }
        if ($where != '') {
            $sql .= " WHERE " . $where;
        }
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        foreach ($list->result_array() as $row => $value) {
            $rows = array();
            $rows[] = $value['param'];
            $rows[] = $value['x1'];
            $rows[] = $value['x2'];
            $rows[] = $value['x3'];
            $rows[] = $value['x4'];
            $rows[] = $value['x5'];
            $rows[] = $value['b1'];
            $rows[] = $value['z'];
            $rows[] = $value['b2'];
            $option['data'][] = $rows;
        }
        echo json_encode($option);
    }

    function Datatables_Pelatihan($dt) {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $sql = "SELECT {$columns} FROM {$dt['table']}";
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';
        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $where .= $columnd[$i] . ' LIKE "%' . $search . '%"';

                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }
        if ($where != '') {
            $sql .= " WHERE " . $where;
        }
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        foreach ($list->result_array() as $row => $value) {
            $rows = array();
            $rows[] = 'L-' . $value['id_lokasi'];
            $rows[] = $value['nama_lokasi'];
            $rows[] = $value['data1'];
            $rows[] = $value['data2'];
            $rows[] = $value['data3'];
            $rows[] = $value['data4'];
            $rows[] = $value['data5'];
            $rows[] = $value['data6'];
            $option['data'][] = $rows;
        }
        echo json_encode($option);
    }

    function Datatables_Pengujian($dt) {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $sql = "SELECT {$columns} FROM {$dt['table']}";
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';
        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $where .= $columnd[$i] . ' LIKE "%' . $search . '%"';

                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }
        if ($where != '') {
            $sql .= " WHERE " . $where;
        }
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        foreach ($list->result_array() as $row => $value) {
            $rows = array();
            $rows[] = 'L-' . $value['id_lokasi'];
            $rows[] = $value['nama_lokasi'];
            $rows[] = $value['data1'];
            $rows[] = $value['data2'];
            $rows[] = $value['data3'];
            $rows[] = $value['data4'];
            $rows[] = $value['data5'];
            $rows[] = $value['data6'];
            $option['data'][] = $rows;
        }
        echo json_encode($option);
    }

    function Datatables_Transformasi($dt) {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $sql = "SELECT {$columns} FROM {$dt['table']}";
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';
        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $where .= $columnd[$i] . ' LIKE "%' . $search . '%"';

                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }
        if ($where != '') {
            $sql .= " WHERE " . $where;
        }
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        foreach ($list->result_array() as $row => $value) {
            $rows = array();
            $rows[] = 'L-' . $value['id_lokasi'];
            $rows[] = $value['nama_lokasi'];
            $rows[] = $value['data1'];
            $rows[] = $value['data2'];
            $rows[] = $value['data3'];
            $rows[] = $value['data4'];
            $rows[] = $value['data5'];
            $rows[] = $value['data6'];
            $option['data'][] = $rows;
        }
        echo json_encode($option);
    }

    function Datatables_Transformasi_1($dt) {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $sql = "SELECT {$columns} FROM {$dt['table']}";
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';
        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $where .= $columnd[$i] . ' LIKE "%' . $search . '%"';

                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }
        if ($where != '') {
            $sql .= " WHERE " . $where;
        }
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        foreach ($list->result_array() as $row => $value) {
            $rows = array();
            $rows[] = 'L-' . $value['id_lokasi'];
            $rows[] = $value['nama_lokasi'];
            $rows[] = $value['data1'];
            $rows[] = $value['data2'];
            $rows[] = $value['data3'];
            $rows[] = $value['data4'];
            $rows[] = $value['data5'];
            $rows[] = $value['data6'];
            $option['data'][] = $rows;
        }
        echo json_encode($option);
    }

    function Datatables_Koordinat($dt) {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $sql = "SELECT {$columns} FROM {$dt['table']}";
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';
        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $where .= $columnd[$i] . ' LIKE "%' . $search . '%"';

                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }
        if ($where != '') {
            $sql .= " WHERE " . $where;
        }
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        foreach ($list->result_array() as $row => $value) {
            $rows = array();
            $rows[] = 'Koor-' . $value['id'];
            $rows[] = $value['name'];
            $rows[] = $value['lat'];
            $rows[] = $value['lng'];
            $option['data'][] = $rows;
        }
        echo json_encode($option);
    }

}