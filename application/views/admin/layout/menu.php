<div class="sidebar-nav">
    <ul>
    <li><a href="#" data-target=".dasboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="glyphicon glyphicon-home"></i> Dashboard <i class="fa fa-collapse"></i></a></li>
        <li>
            <ul class="dasboard-menu nav nav-list collapse">
                <li ><a id="pointer" onclick="getDashboard();" ><span class="fa fa-dashboard"></span> Dashboard</a></li>
            </ul>
    </li><li><a href="#" data-target=".akun-menu" class="nav-header collapsed" data-toggle="collapse"><i class="glyphicon glyphicon-lock"></i> Akun <i class="fa fa-collapse"></i></a></li>
        <li>
            <ul class="akun-menu nav nav-list collapse">
                <li ><a id="pointer" onclick="getUser();" ><span class="glyphicon glyphicon-user"></span> User</a></li>
            </ul>
    </li>
    <li><a href="#" data-target=".pelatihan-menu" class="nav-header collapsed" data-toggle="collapse"><i class="glyphicon glyphicon-list"></i> Data <i class="fa fa-collapse"></i></a></li>
        <li>
            <ul class="pelatihan-menu nav nav-list collapse">
                <li ><a id="pointer" onclick="getPelatihan();" ><span class="fa fa-table"></span> Data Pelatihan</a></li>
                <li ><a id="pointer" onclick="getPengujian();" ><span class="fa fa-table"></span> Data Pengujian</a></li>
                <li ><a id="pointer" onclick="getBobot();" ><span class="fa fa-table"></span> Bobot & Bias</a></li>
                <li ><a id="pointer" onclick="koordinat();" ><span class="fa fa-table"></span> Koordinat Lokasi</a></li>
            </ul>
           
    </li>
    <li><a href="#" data-target=".backpro-menu" class="nav-header collapsed" data-toggle="collapse"><i class="glyphicon glyphicon-stats"></i> Backpropagation <i class="fa fa-collapse"></i></a></li>
        <li>
            <ul class="backpro-menu nav nav-list collapse">
                <li ><a id="pointer" onclick="getPembelajaran();" ><span class="glyphicon glyphicon-hourglass"></span> Proses Pembelajaran</a></li>
                <li ><a id="pointer" onclick="getBobotAkhir();" ><span class="fa fa-table"></span> Bobot & Bias Akhir</a></li>
                <li ><a id="pointer" onclick="getSimpe();" ><span class="fa fa-bar-chart-o"></span> Simulasi Data Pelatihan</a></li>
                <li ><a id="pointer" onclick="getSimpe_1();" ><span class="fa fa-bar-chart-o"></span> Simulasi Data Pengujian</a></li>
            </ul>
    </li>
    <li><a href="#" data-target=".analisis-menu" class="nav-header collapsed" data-toggle="collapse"><i class="glyphicon glyphicon-stats"></i> Analisis <i class="fa fa-collapse"></i></a></li>
        <li>
            <ul class="analisis-menu nav nav-list collapse">
                <li ><a id="pointer" onclick="getAnalisis();" ><span class="fa fa-bar-chart-o"></span> Pembelajaran</a></li>
                <li ><a id="pointer" onclick="getInisialisasi();" ><span class="fa fa-bar-chart-o"></span> Inisialisasi Bobot</a></li>
                <li ><a id="pointer" onclick="getSimpelAnalis();" ><span class="fa fa-bar-chart-o"></span> Simulasi Data Pelatihan</a></li>
                <li ><a id="pointer" onclick="getSimpengAnalis();" ><span class="fa fa-bar-chart-o"></span> Simulasi Data Pengujian</a></li>
            </ul>
    </li>
    <li><a href="#" data-target=".hasil-menu" class="nav-header collapsed" data-toggle="collapse"><i class="glyphicon glyphicon-stats"></i> Hasil Prediksi<i class="fa fa-collapse"></i></a></li>
        <li>
            <ul class="hasil-menu nav nav-list collapse">
                <li ><a id="pointer" onclick="getHasil();" ><span class="fa fa-bar-chart-o"></span> Persentase Wilayah</a></li>
                <li ><a id="pointer" onclick="getMaps();" ><span class="glyphicon glyphicon-map-marker"></span> Pemetaan Lokasi</a></li>
            </ul>
    </li>
    <li><a href="#" data-target=".reset-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-warning modal-icon"></i> Reset Data <i class="fa fa-collapse"></i></a></li>
        <li>
            <ul class="reset-menu nav nav-list collapse">
                <li ><a id="pointer" onclick="reset_data_1()" ><span class="fa fa-refresh"></span> Data Pembelajaran</a></li>
                <li ><a id="pointer" onclick="reset_data_2()" ><span class="fa fa-refresh"></span> Data Master</a></li>
            </ul>
            </ul>
    </li>
    </ul>

</div>

<script type="text/javascript">
    $("li a").on("click",function(){
        $(".selected").removeClass("selected");
        $(this).addClass("selected");
    });
</script>

<style type="text/css">
    .selected{
        background: #ccc;
    }
</style>